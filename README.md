# Duplicate the repo

Create a new repo and duplicate the files.

Change all tenant-dev to tenant-dev.
Rename any tenant-dev files to tenant-dev.

# Source env.sh

Source the env.sh file - this is where I keep mine:

```bash
source ~/gitlab.com/dedevsecops/k8s-eks-template/env.sh
```

# Create cluster with eksctl

```bash
export AWS_DEFAULT_REGION="eu-west-3"
export AWS_PROFILE="CoDeFi"
#eksctl create cluster -n tenant-dev -r eu-west-3 --external-dns-access --alb-ingress-access --version 1.13
eksctl create cluster -n tenant-dev
```

# Get kubeconfig

eksctl utils write-kubeconfig -n tenant-dev --kubeconfig kubeconfig

# Encrypt storage class

```bash
kubectl get sc gp2 --export -o json | jq ".parameters += {encrypted:\"true\"}" > sc_gp2_encrypted.json
kubectl delete sc gp2
kubectl apply -f sc_gp2_encrypted.json --record
rm sc_gp2_encrypted.json
```

# Install Tiller

TODO: Either get [TLS](https://github.com/helm/helm/blob/master/docs/tiller_ssl.md) working or wait for tillerless to be the norm.

```bash
kubectl apply -f tiller.yaml --record
helm init --service-account tiller --override 'spec.template.spec.containers[0].command'='{/tiller,--storage=secret}'
helm repo update
```

# Create Route53 zone

```bash
export CLUSTER_NAME=tenant-dev
export DOMAIN_NAME=dedevsecops.com
aws route53 create-hosted-zone --name "${CLUSTER_NAME}.${DOMAIN_NAME}." --caller-reference "${CLUSTER_NAME}-$(date +%s)"
export ZONE_ID=$(aws route53 list-hosted-zones-by-name --output json --dns-name "${CLUSTER_NAME}.${DOMAIN_NAME}." | jq -r '.HostedZones[0].Id')
aws route53 list-resource-record-sets --output json --hosted-zone-id "${ZONE_ID}" \
 --query "ResourceRecordSets[?Type == 'NS']" | jq -r '.[0].ResourceRecords[].Value'
```

Log into whatever service you use to manage DNS for ${DOMAIN_NAME}, and create multiple NS records for the cluster's subdomain (in this example ${CLUSTER_NAME}.${DOMAIN_NAME}) - one for each NS record that's output by the command above.

# Getting Gitlab CI working

Create "gitlab-ci" programmatic AWS User. Put user's AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY in https://gitlab.com/groups/dedevsecops/-/settings/ci_cd as masked variables.

Add all AWS users that you want to have admin access to the cluster to this:

kubectl edit cm -n kube-system aws-auth

Add:

  mapUsers: |
    - userarn: arn:aws:iam::085634892402:user/gitlab-ci
      username: admin
      groups:
        - system:masters

eksctl utils write-kubeconfig --name=tenant-dev --kubeconfig=./tenant-dev.conf
Change aws-iam-authenticator to heptio-authenticator-aws.


# kube2iam

Grab the nodegroup role and add it to the assume.json file.

aws iam list-roles | grep tenant-dev

```bash
aws iam create-role --role-name kube2iam_external-dns_tenant-dev --assume-role-policy-document file://kube2iam_external-dns_tenant-dev.assume.json
aws iam put-role-policy --role-name kube2iam_external-dns_tenant-dev --policy-name kube2iam_external-dns_tenant-dev --policy-document file://kube2iam_external-dns_tenant-dev.policy.json
```

# Install cert-manager CRDs

Based on https://hub.helm.sh/charts/jetstack/cert-manager

Need to modify Kong before using cert-manager. See https://github.com/ollystephens/acme-kong-kube-helper.

kubectl patch deployment kong-kong-controller --patch "$(cat acme-kong-kube-helper.yaml)"

Add env vars to the admin-api container in

kubectl edit deploy kong-kong-controller

```yaml
        - name: CLIENT_ID
          value: 0W60wylnaZyQeY9PiwYIVPSrQLgHQ11C
        - name: DISCOVERY_URL
          value: https://tenant-dev.eu.auth0.com/.well-known/openid-configuration
        - name: CLIENT_SECRET
          valueFrom:
            secretKeyRef:
              key: client-secret
              name: auth0-client
```

kubectl apply -f cert-manager.yaml --record
kubectl label namespace default certmanager.k8s.io/disable-validation=true

After installing cert-manager:
kubectl apply -f clusterissuer.yaml --record

# kapprover

We don't need this if we aren't using the vault chart.

TODO: Why do we have to delete all the filter and denier lines? That seems bad.

Based on https://kubernetes.io/docs/tasks/tls/managing-tls-in-a-cluster/

Clone https://github.com/proofpoint/kapprover
Delete all the filter and denier lines from resources/deployment.yaml
kubectl apply -f resources --record

# Cheatsheets

## Helm

helm ls -a
helm del --purge <release>

## Kong

### Get routes
```bash
source ~/gitlab.com/dedevsecops/k8s-eks-template/env.sh
kubectl exec -it $(kubectl get pods -lapp=kong -lcomponent=app -o json | jq -r '.items[].metadata.name') -- curl -k https://localhost:8444/routes | jq .
```

## Kubernetes

```bash
source ~/gitlab.com/dedevsecops/k8s-eks-template/env.sh
kubectl get pods -n cofi
kubectl describe pods -n cofi
```

# Troubleshooting

## Pod isn't ready



```bash
# Source the env.sh file
source ~/gitlab.com/dedevsecops/k8s-eks-template/env.sh
# Find the pod name
kubectl get pods -n cofi
# Describe the pod. This should show the problem, like a readiness probe fail, and it will have the port that the service is running on.
kubectl describe pods -n cofi $pod
# Find the readiness probe - it's likely just an HTTP request to /healthcheck, but not always...
kubectl get pods -n cofi -o yaml $pod
# Run the readiness probe manually (for node apps, port is likely 3000, but describe command is authoritative.)
kubectl exec -it $pod -- curl http://localhost:$port/healthcheck
```
